package com.example.wipro.exception;

public class ResourceNotFoundException extends Exception {
    public ResourceNotFoundException(String message){
        super(message);
    }

    @Override
    public String toString() {
        return "Resource not found exception";
    }
}
