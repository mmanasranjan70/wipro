package com.example.wipro.exception;

public class BadResourceException extends Exception {
    private String errorMessage;

    public BadResourceException(String message) {
        super(message);
    }

    @Override
    public String toString() {
        return errorMessage;
    }

    public void addErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
