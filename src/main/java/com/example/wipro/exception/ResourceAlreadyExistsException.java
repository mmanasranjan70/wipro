package com.example.wipro.exception;

public class ResourceAlreadyExistsException extends Exception {
    public ResourceAlreadyExistsException(String message){
        super(message);
    }

    @Override
    public String toString() {
        return "Resource Already Exists Exception";
    }
}
