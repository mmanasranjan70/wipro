package com.example.wipro.service;

import com.example.wipro.exception.BadResourceException;
import com.example.wipro.exception.ResourceAlreadyExistsException;
import com.example.wipro.exception.ResourceNotFoundException;
import com.example.wipro.model.PhoneDirectory;
import com.example.wipro.repository.PhoneRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

@Service
public class PhoneDirectoryServiceImpl implements PhoneDirectoryService {
    @Autowired
    private PhoneRepository phoneRepository;

    private boolean existsById(Long id) {
        return phoneRepository.existsById(id);
    }

    @Override
    public PhoneDirectory findById(Long id) throws ResourceNotFoundException {
        PhoneDirectory contact = phoneRepository.findById(id).orElse(null);
        if (contact == null) {
            throw new ResourceNotFoundException("Cannot find Contact with id: " + id);
        } else return contact;
    }

    @Override
    public List<PhoneDirectory> findAll(int pageNumber, int rowPerPage) {
        List<PhoneDirectory> contacts = new ArrayList<>();
        Pageable sortedByIdAsc = PageRequest.of(pageNumber - 1, rowPerPage,
                Sort.by("id").ascending());
        phoneRepository.findAll(sortedByIdAsc).forEach(contacts::add);
        return contacts;
    }

    @Override
    public PhoneDirectory save(PhoneDirectory phoneDirectory) throws BadResourceException, ResourceAlreadyExistsException {
        if (!StringUtils.isEmpty(phoneDirectory.getName())) {
            if (phoneDirectory.getId() != null && existsById(phoneDirectory.getId())) {
                throw new ResourceAlreadyExistsException("Contact with id: " + phoneDirectory.getId() +
                        " already exists");
            }
            return phoneRepository.save(phoneDirectory);
        } else {
            BadResourceException exc = new BadResourceException("Failed to save contact");
            exc.addErrorMessage("Contact is null or empty");
            throw exc;
        }
    }

    @Override
    public void update(PhoneDirectory phoneDirectory)
            throws BadResourceException, ResourceNotFoundException {
        if (!StringUtils.isEmpty(phoneDirectory.getName())) {
            if (!existsById(phoneDirectory.getId())) {
                throw new ResourceNotFoundException("Cannot find Contact with id: " + phoneDirectory.getId());
            }
            phoneRepository.save(phoneDirectory);
        } else {
            BadResourceException exc = new BadResourceException("Failed to save contact");
            exc.addErrorMessage("Contact is null or empty");
            throw exc;
        }
    }

    @Override
    public void deleteById(Long id) throws ResourceNotFoundException {
        if (!existsById(id)) {
            throw new ResourceNotFoundException("Cannot find contact with id: " + id);
        } else {
            phoneRepository.deleteById(id);
        }
    }

    @Override
    public Long count() {
        return phoneRepository.count();
    }

    @Override
    public List<String> search(String keyword) {
        return phoneRepository.search(keyword);
    }
}
