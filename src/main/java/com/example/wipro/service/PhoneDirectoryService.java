package com.example.wipro.service;

import com.example.wipro.exception.BadResourceException;
import com.example.wipro.exception.ResourceAlreadyExistsException;
import com.example.wipro.exception.ResourceNotFoundException;
import com.example.wipro.model.PhoneDirectory;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

@Service
public interface PhoneDirectoryService {
    public PhoneDirectory findById(Long id) throws ResourceNotFoundException;

    public List<PhoneDirectory> findAll(int pageNumber, int rowPerPage);

    public PhoneDirectory save(PhoneDirectory phoneDirectory) throws BadResourceException, ResourceAlreadyExistsException;

    public void update(PhoneDirectory phoneDirectory)
            throws BadResourceException, ResourceNotFoundException;

    public void deleteById(Long id) throws ResourceNotFoundException;

    public Long count();

    public List<String> search(String keyword);
}
