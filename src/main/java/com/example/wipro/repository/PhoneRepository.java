package com.example.wipro.repository;

import com.example.wipro.model.PhoneDirectory;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PhoneRepository extends PagingAndSortingRepository<PhoneDirectory, Long>,
        JpaSpecificationExecutor<PhoneDirectory> {
    @Query("SELECT name FROM PhoneDirectory where name like %:keyword%")
    public List<String> search(@Param("keyword") String keyword);
}
