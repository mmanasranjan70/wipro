package com.example.wipro.controller;

import com.example.wipro.exception.ResourceNotFoundException;
import com.example.wipro.model.PhoneDirectory;
import com.example.wipro.service.PhoneDirectoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class PhoneDirectoryController {
    @Value("${msg.title}")
    private String title;
    @Value("${row.per.page}")
    private int ROW_PER_PAGE;

    @Autowired
    private PhoneDirectoryService phoneDirectoryService;

    @GetMapping(value = "/check_health")
    public String healthCheck(){
        return "Ok";
    }

    @GetMapping(value = {"/", "/index"})
    public String index(Model model) {
        model.addAttribute("title", title);
        return "index";
    }

    @RequestMapping(value = "search", method = RequestMethod.GET)
    @ResponseBody
    public List<String> search(HttpServletRequest request) {
        return phoneDirectoryService.search(request.getParameter("term"));
    }

    @GetMapping(value = "/contacts")
    public String getContacts(Model model,
                              @RequestParam(value = "page", defaultValue = "1") int pageNumber) {
        List<PhoneDirectory> contacts = phoneDirectoryService.findAll(pageNumber, ROW_PER_PAGE);

        long count = phoneDirectoryService.count();
        boolean hasPrev = pageNumber > 1;
        boolean hasNext = (pageNumber * ROW_PER_PAGE) < count;
        model.addAttribute("contacts", contacts);
        model.addAttribute("hasPrev", hasPrev);
        model.addAttribute("prev", pageNumber - 1);
        model.addAttribute("hasNext", hasNext);
        model.addAttribute("next", pageNumber + 1);
        return "contact-list";
    }

    @GetMapping(value = {"/contacts/add"})
    public String showAddContact(Model model) {
        PhoneDirectory contact = new PhoneDirectory();
        model.addAttribute("add", true);
        model.addAttribute("contact", contact);

        return "contact-edit";
    }

    @PostMapping(value = "/contacts/add")
    public String addContact(Model model,
                             @ModelAttribute("contact") PhoneDirectory phoneDirectory) {
        try {
            PhoneDirectory newContact = phoneDirectoryService.save(phoneDirectory);
            return "redirect:/contacts/" + String.valueOf(newContact.getId());
        } catch (Exception ex) {
            // log exception first,
            // then show error
            String errorMessage = ex.getMessage();
//            logger.error(errorMessage);
            model.addAttribute("errorMessage", errorMessage);

            //model.addAttribute("contact", contact);
            model.addAttribute("add", true);
            return "contact-edit";
        }
    }

    @GetMapping(value = {"/contacts/{contactId}/edit"})
    public String showEditContact(Model model, @PathVariable long contactId) {
        PhoneDirectory contact = null;
        try {
            contact = phoneDirectoryService.findById(contactId);
        } catch (ResourceNotFoundException ex) {
            model.addAttribute("errorMessage", "Contact not found");
        }
        model.addAttribute("add", false);
        model.addAttribute("contact", contact);
        return "contact-edit";
    }

    @PostMapping(value = {"/contacts/{contactId}/edit"})
    public String updateContact(Model model,
                                @PathVariable long contactId,
                                @ModelAttribute("contact") PhoneDirectory contact) {
        try {
            contact.setId(contactId);
            phoneDirectoryService.update(contact);
            return "redirect:/contacts/" + String.valueOf(contact.getId());
        } catch (Exception ex) {
            // log exception first,
            // then show error
            String errorMessage = ex.getMessage();
//            logger.error(errorMessage);
            model.addAttribute("errorMessage", errorMessage);

            model.addAttribute("add", false);
            return "contact-edit";
        }
    }

    @GetMapping(value = "/contacts/{contactId}")
    public String getContactById(Model model, @PathVariable long contactId) {
        PhoneDirectory contact = null;
        try {
            contact = phoneDirectoryService.findById(contactId);
        } catch (ResourceNotFoundException ex) {
            model.addAttribute("errorMessage", "Contact not found");
        }
        model.addAttribute("contact", contact);
        return "contact";
    }

    @GetMapping(value = {"/contacts/{contactId}/delete"})
    public String showDeleteContactById(
            Model model, @PathVariable long contactId) {
        PhoneDirectory contact = null;
        try {
            contact = phoneDirectoryService.findById(contactId);
        } catch (ResourceNotFoundException ex) {
            model.addAttribute("errorMessage", "Contact not found");
        }
        model.addAttribute("allowDelete", true);
        model.addAttribute("contact", contact);
        return "contact";
    }

    @PostMapping(value = {"/contacts/{contactId}/delete"})
    public String deleteContactById(
            Model model, @PathVariable long contactId) throws ResourceNotFoundException {
        try {
            phoneDirectoryService.deleteById(contactId);
            return "redirect:/contacts";
        } catch (ResourceNotFoundException ex) {
            String errorMessage = ex.getMessage();
//            logger.error(errorMessage);
            model.addAttribute("errorMessage", errorMessage);
            return "contact";
        }
    }
}
